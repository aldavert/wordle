#include <iostream>
#include <vector>

int main(int /*argc*/, char * * /*argv*/)
{
    //constexpr unsigned int n = 1'000'000'000;
    constexpr unsigned long n = 10'000'000;
    std::vector<bool> primes(n);
    primes[0] = primes[1] = false;
    for (unsigned long i = 2; i < n; ++i)
        primes[i] = true;
    for (unsigned long i = 2; i < n; ++i)
    {
        if (primes[i])
        {
            for (unsigned long j = i + i; j < n; j += i)
                primes[j] = false;
        }
    }
    for (unsigned long i = 0; i < n; ++i)
        if (primes[i])
            std::cout << i << '\n';
    return 0;
}
