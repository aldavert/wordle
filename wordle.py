import urwid
import random
import string
import operator
import math
import unidecode
import glob
import sys
from enum import Enum

# TODO: Think of adding a definition based mode where a partial definition of the word is shown at the start.
#       Then, each word tested will uncover parts of the definition. This can be better to learn words from different languages.
# TODO: Make a continuous model with a score and new rounds start automatically after wining or failing to discover the input word.
# TODO: Split the different menus into different classes to better separate the different modes.

class SwitchingPadding(urwid.Padding):
    def padding_values(self, size, focus):
        maxcol = size[0]
        width, ignore = self.original_widget.pack(size, focus=focus)
        self.align = "center"
        return urwid.Padding.padding_values(self, size, focus)

def loadVocabulary(filename):
    valid_characters = set(string.ascii_lowercase + 'ç' + '0123456789')
    vocabulary = {}
    try:
        f = open(filename, 'r')
        for word in f.read().split():
            word_short = word.lower()
            c_cedilles = list(filter(lambda x: x[1] == 'ç', enumerate(word_short)))
            word_short = list(unidecode.unidecode(word_short))
            for pos, char in c_cedilles:
                word_short[pos] = char
            word_short = ''.join(filter(lambda x: x in valid_characters, word_short))
            if len(word_short) in vocabulary:
                vocabulary[len(word_short)][word_short] = word
            else:
                vocabulary[len(word_short)] = { word_short : word }
        f.close()
    except:
        pass
    return vocabulary

def loadLanguage(filename):
    interface_strings = {}
    try:
        f = open(filename, 'r')
        for line in f.readlines():
            if len(line) <= 1:
                continue
            line = line.split()
            if len(line) <= 1:
                continue
            interface_strings[line[0]] = ' '.join(line[1:])
    except:
        interface_strings['STATUS_EXIT'] = 'Exit'
        interface_strings['STATUS_MENU'] = 'Configuration menu'
        interface_strings['STATUS_BACK'] = 'Back'
        interface_strings['STATUS_UP'] = 'Up'
        interface_strings['STATUS_DOWN'] = 'Down'
        interface_strings['STATUS_SELECT'] = 'Select'
        interface_strings['MENU_OPTIONS'] = 'Options'
        interface_strings['MENU_MODE'] = 'Game type'
        interface_strings['MENU_NEW_GAME'] = 'New Game'
        interface_strings['MENU_RESET'] = 'Reset'
        interface_strings['MENU_HIDE'] = 'Hide hints'
        interface_strings['MENU_SHOW'] = 'Show hints'
        interface_strings['MENU_ATTEMPTS'] = 'Numbers of attempts'
        interface_strings['MENU_LENGTH'] = 'Word length'
        interface_strings['MENU_VOCABULARY'] = 'Change Vocabulary'
        interface_strings['MENU_LANGUAGE'] = 'Change Language'
        interface_strings['HINTS_SUGGESTIONS'] = 'Suggestions'
        interface_strings['DISPLAY_ATTEMPTS'] = 'Number of attempts'
        interface_strings['DISPLAY_LENGTH'] = 'Word length'
        interface_strings['STATUS_CANNOT_OPEN'] = 'File cannot be opened'
        interface_strings['MENU_TYPE_NORMAL'] = 'Normal mode'
        interface_strings['MENU_TYPE_SOLVER'] = 'Solver mode'
    return interface_strings

class Interface:
    class Status(Enum):
        GAME = 0
        SOLVER = 1
        MENU = 2
        DIALOG_ATTEMPTS = 3
        DIALOG_LENGTH = 4
    def __init__(self, word_length = 5, number_of_attempts = 6, filename_vocabulary = None, filename_language = None, game_mode = None):
        # Initialize problem variables.
        self._filename_language = filename_language
        self._interface_strings = loadLanguage(filename_language)
        self._valid_characters = string.ascii_lowercase + 'ç' + '0123456789'
        self._number_of_attempts = number_of_attempts
        self._filename_vocabulary = filename_vocabulary
        if filename_vocabulary != None:
            self._dictionary = loadVocabulary(filename_vocabulary)
        else:
            self._dictionary = {}
        self._word_length = word_length
        if self._dictionary and self._word_length in self._dictionary:
            self._putative_words = self._dictionary[self._word_length].keys()
        else:
            self._putative_words = []
        self._introduced_words = set()
        # Initialize interface constant variables.
        self._font = urwid.HalfBlock5x4Font()
        self._font_height = self._font.height
        self._font_width = max((self._font.char_width(x) for x in (self._valid_characters + '&')))
        self._status = game_mode
        self._play_mode = self._status
        self._enter_letter = True
        palette = [
            ('good'     , '', '', '', '#000000', '#00AA00'),
            ('missing'  , '', '', '', '#707070', '#404040'),
            ('normal'   , '', '', '', '#000000', '#FFFFFF'),
            ('wrong'    , '', '', '', '#000000', '#CC9900'),
            ('unlabeled', '', '', '', '#FFFFFF', '#202020'),
            ('keyboard' , '', '', '', '#FFFFFF', '' ),
            ('kb_miss'  , '', '', '', '#000000', '' ),
            ('kb_good'  , '', '', '', '#000000', '#00BB00'),
            ('kb_wrong' , '', '', '', '#000000', '#CC9900'),
            ('highlight', '', '', '', '#AA0000,bold', ''),
            ('status'   , '', '', '', '#252525,bold', '#DD9900'),
            ('background', '', '', '', '#505050', '#303030'),
            ('text', '', '', '', '', '' )
            ]
        self._hints = True
        # Draw interface.
        self.draw()
        # Main loop
        self.chooseSolution()
        self._loop = urwid.MainLoop(self._background, palette, unhandled_input=self.unhandled_input)
        self._loop.screen.set_terminal_properties(colors = 2**24)
    """
    Draws the game interface.
    """
    def draw(self):
        self._active_row = 0
        self._active_char = 0
        self._letters_values = []
        self._letters_wrap = []
        column_letters = []
        for i in range(self._number_of_attempts):
            current_letters = []
            current_wrap = []
            current_column = []
            for j in range(self._word_length):
                current_letters.append(urwid.BigText("", self._font))
                bt = SwitchingPadding(current_letters[-1], 'center', None)
                bt = urwid.AttrWrap(bt, 'normal')
                current_wrap.append(bt)
                current_column.append((self._font_width + 2, bt))
            column_letters.append(urwid.Columns(current_column))
            self._letters_values.append(current_letters)
            self._letters_wrap.append(current_wrap)
        self._keyboard = {}
        for c in self._valid_characters:
            self._keyboard[c] = urwid.Text(('keyboard', c), 'center')
        keyboard = []
        keyboard.append(urwid.Columns(((3, self._keyboard[c]) for c in '0123456789')))
        keyboard.append(urwid.Columns(((3, self._keyboard[c]) for c in 'qwertyuiop')))
        keyboard.append(urwid.Columns(((3, self._keyboard[c]) for c in 'asdfghjklç')))
        keyboard.append(urwid.Columns(((3, self._keyboard[c]) for c in 'zxcvbnm')))
        input_interface = urwid.Pile(column_letters)
        pad_input = max(0, (10 * 3 - self._word_length * (self._font_width + 2)) / 2)
        pad_keys = max(0, (self._word_length * (self._font_width + 2) - 10 * 3) / 2)
        left_width = max(10 * 3, self._word_length * (self._font_width + 2))
        left_interface = urwid.Pile([urwid.Padding(input_interface, left = pad_input),
                                     urwid.Padding(urwid.Pile(keyboard), left = pad_keys)])
        if self._hints:
            show_text = ', '.join(sorted(random.sample(list(self._putative_words), min(len(self._putative_words), 300))))
        else:
            show_text = ''
        self._words_available = urwid.Text(show_text, 'center', 'space')
        self._status_bar = urwid.Text((f' [ESC/F10] {self._interface_strings["STATUS_EXIT"]} | [F3] {self._interface_strings["STATUS_MENU"]}'))
        self._interface = urwid.Columns([(left_width, left_interface), self._words_available], dividechars=1)
        self._interface = urwid.Pile([
            (4 + self._number_of_attempts * self._font_height, urwid.Filler(self._interface)),
            (1, urwid.AttrMap(urwid.Filler(self._status_bar), 'status'))])
        self._interface = urwid.Filler(self._interface)
        self._letters_possible = [set(self._valid_characters) for i in range(self._word_length)]
        self._letters_present = set()
        self._background = urwid.Overlay(self._interface,
                urwid.AttrMap(urwid.SolidFill(u'\N{Box Drawings Light Vertical and Horizontal}'), 'background'),
                'center', self._word_length * (self._font_width + 2) + 4 + (self._word_length + 2) * 7,
                'middle', self._number_of_attempts * self._font_height + 5)
    """
    Run the interface.
    """
    def run(self):
        self._loop.run()
    """
    Process the keys pressed by the user.
    """
    def unhandled_input(self, key):
        if key == 'f10':
            raise urwid.ExitMainLoop()
        if self._status == self.Status.GAME or self._status == self.Status.SOLVER:
            if len(key) == 1 and ((key >= 'a' and key <= 'z') or key == 'ç' or (key >= '0' and key <= '9')):
                self.addLetter(key)
            elif key == 'backspace':
                self.removeLetter()
            elif key == 'enter':
                if self._active_row < self._number_of_attempts and  self._active_char == self._word_length:
                    self.checkWord()
            elif key == 'f3':
                self.changeMode(self.Status.MENU)
            elif key == 'esc':
                raise urwid.ExitMainLoop()
        elif self._status == self.Status.MENU:
            if key == 'esc':
                self.changeMode(self._play_mode)
            elif key == 'k':
                self._menu.keypress((50, self._menu_height), 'up')
            elif key == 'j':
                self._menu.keypress((50, self._menu_height), 'down')
        elif self._status == self.Status.DIALOG_ATTEMPTS:
            if key == 'esc':
                self.changeMode(self._play_mode)
            elif key == 'enter':
                number_of_attempts = int(self._edit_attempts.get_edit_text())
                if number_of_attempts == self._number_of_attempts:
                    self.changeMode(self._play_mode)
                elif number_of_attempts > 0 and number_of_attempts <= self._word_length * 2:
                    self._number_of_attempts = number_of_attempts
                    self.draw()
                    self.reset()
        elif self._status == self.Status.DIALOG_LENGTH:
            if key == 'esc':
                self.changeMode(self._play_mode)
            elif key == 'enter':
                word_length = int(self._edit_attempts.get_edit_text())
                if word_length == self._word_length:
                    self.changeMode(self._play_mode)
                elif word_length in self._dictionary:
                    self._word_length = word_length
                    self.draw()
                    self.restart()
    """
    Changes the main interface mode to Game, Solver or Menu.
    """
    def changeMode(self, status):
        if status == self.Status.GAME or status == self.Status.SOLVER:
            self._status_bar.set_text(f' [ESC/F10] {self._interface_strings["STATUS_EXIT"]} | [F3] {self._interface_strings["STATUS_MENU"]}')
            self._loop.widget = self._background
            self._status = status
        elif status == self.Status.MENU:
            self._status_bar.set_text(f' [ESC] {self._interface_strings["STATUS_BACK"]}'
                                    + f' | [F10] {self._interface_strings["STATUS_EXIT"]}'
                                    + f' | [k] {self._interface_strings["STATUS_UP"]}'
                                    + f' | [j] {self._interface_strings["STATUS_DOWN"]}'
                                    + f' | [Enter] {self._interface_strings["STATUS_SELECT"]}')
            body = [urwid.Text(self._interface_strings['MENU_OPTIONS']), urwid.Divider('─')]
            button = urwid.Button(self._interface_strings['MENU_MODE'])
            urwid.connect_signal(button, 'click', self.changeGameType)
            body.append(urwid.AttrMap(button, None, focus_map = 'reversed'))
            # -----------------------------------------------------------------
            button = urwid.Button(self._interface_strings['MENU_NEW_GAME'])
            urwid.connect_signal(button, 'click', self.restart)
            body.append(urwid.AttrMap(button, None, focus_map = 'reversed'))
            # -----------------------------------------------------------------
            button = urwid.Button(self._interface_strings['MENU_RESET'])
            urwid.connect_signal(button, 'click', self.reset)
            body.append(urwid.AttrMap(button, None, focus_map = 'reversed'))
            # -----------------------------------------------------------------
            if self._hints:
                button = urwid.Button(self._interface_strings['MENU_HIDE'])
            else:
                button = urwid.Button(self._interface_strings['MENU_SHOW'])
            urwid.connect_signal(button, 'click', self.enableHints)
            body.append(urwid.AttrMap(button, None, focus_map = 'reversed'))
            # -----------------------------------------------------------------
            button = urwid.Button(self._interface_strings['MENU_ATTEMPTS'])
            urwid.connect_signal(button, 'click', self.changeNumberAttempts)
            body.append(urwid.AttrMap(button, None, focus_map = 'reversed'))
            # -----------------------------------------------------------------
            button = urwid.Button(self._interface_strings['MENU_LENGTH'])
            urwid.connect_signal(button, 'click', self.changeWordLength)
            body.append(urwid.AttrMap(button, None, focus_map = 'reversed'))
            # -----------------------------------------------------------------
            button = urwid.Button(self._interface_strings['MENU_VOCABULARY'])
            urwid.connect_signal(button, 'click', self.changeVocabulary)
            body.append(urwid.AttrMap(button, None, focus_map = 'reversed'))
            # -----------------------------------------------------------------
            button = urwid.Button(self._interface_strings['MENU_LANGUAGE'])
            urwid.connect_signal(button, 'click', self.changeLanguage)
            body.append(urwid.AttrMap(button, None, focus_map = 'reversed'))
            # -----------------------------------------------------------------
            self._menu = urwid.ListBox(urwid.SimpleFocusListWalker(body))
            self._menu_height = self._number_of_attempts * self._font_height + 2
            dialog = urwid.LineBox(urwid.Padding(self._menu, left = 2, right = 2))
            self._loop.widget = urwid.Overlay(dialog,
                    self._background,
                    align = 'center',
                    width = 50,
                    valign = 'middle',
                    height = self._menu_height)
            self._status = status
    """
    Select a new word from the dictionary as the current solution of the game.
    """
    def chooseSolution(self):
        self._solution_histogram = {}
        if self._word_length in self._dictionary:
            self._solution = random.choice(list(self._dictionary[self._word_length].keys()))
            for c in self._solution:
                if not c in self._solution_histogram:
                    self._solution_histogram[c] = 1
                else:
                    self._solution_histogram[c] += 1
        else:
            self._solution = None
    """
    Start a new game.
    """
    def restart(self, button = None):
        self.reset(button)
        self.chooseSolution()
    """
    Removes all the words from the current game to start from the beginning.
    """
    def reset(self, button = None):
        self._putative_words = self._dictionary[self._word_length].keys()
        self._introduced_words = set()
        self._active_row = 0
        self._active_char = 0
        if self._hints:
            show_text = ', '.join(sorted(random.sample(list(self._putative_words), min(len(self._putative_words), 300))))
        else:
            show_text = ''
        self._words_available.set_text(show_text)
        for row in range(self._number_of_attempts):
            for char in range(self._word_length):
                self._letters_wrap[row][char].set_attr('normal')
                self._letters_values[row][char].set_text('')
        for c in self._valid_characters:
            self._keyboard[c].set_text(("keyboard", c))
        self._letters_possible = [set(self._valid_characters) for i in range(self._word_length)]
        self._letters_present = set()
        self.changeMode(self._play_mode)
    """
    Enable or disable the hints on the interface.
    """
    def enableHints(self, button):
        self._hints = not self._hints
        if self._hints:
            if len(self._putative_words) > 2:
                best_text  = f'{self._interface_strings["HINTS_SUGGESTIONS"]}: ' + ', '.join(self.suggestedWords(5)) + '\n'
            else:
                best_text  = f'{self._interface_strings["HINTS_SUGGESTIONS"]}: ' + ', '.join(self._putative_words) + '\n'
            show_text = '-'*20 + '\n' + ', '.join(sorted(random.sample(list(self._putative_words), min(len(self._putative_words), 250))))
            self._words_available.set_text([('highlight', best_text), ('text', show_text)])
        else:
            self._words_available.set_text('')
        self.changeMode(self._play_mode)
    """
    Interface to change the number of attempts of the game.
    """
    def changeNumberAttempts(self, button):
        self._edit_attempts = urwid.IntEdit(u'', self._number_of_attempts)
        options = [urwid.Text(f'{self._interface_strings["DISPLAY_ATTEMPTS"]} (1-{self._word_length * 2}):'), self._edit_attempts]
        dialog =  urwid.LineBox(urwid.ListBox(urwid.SimpleListWalker(options)))
        self._status = self.Status.DIALOG_ATTEMPTS
        self._loop.widget = urwid.Overlay(dialog,
                self._background,
                align = 'center',
                width = 50,
                valign = 'middle',
                height = 4)
    """
    Interface to change the length of the words.
    """
    def changeWordLength(self, button):
        self._edit_attempts = urwid.IntEdit(u'', self._word_length)
        length = sorted(self._dictionary.keys())
        length_grouped = []
        begin = 0
        expected = length[0]
        for end in range(len(length)):
            if length[end] != expected:
                if end - begin > 1:
                    length_grouped.append(f'{length[begin]}-{length[end - 1]}')
                else:
                    length_grouped.append(f'{length[begin]}')
                begin = end
                expected = length[end]
            expected += 1
        if end - begin > 1:
            length_grouped.append(f'{length[begin]}-{length[end]}')
        else:
            length_grouped.append(f'{length[begin]}')
        options = [urwid.Text(f'{self._interface_strings["DISPLAY_LENGTH"]} ({", ".join(length_grouped)}):'), self._edit_attempts]
        self._menu = urwid.ListBox(urwid.SimpleListWalker(options))
        dialog =  urwid.LineBox(self._menu)
        self._status = self.Status.DIALOG_LENGTH
        self._loop.widget = urwid.Overlay(dialog,
                self._background,
                align = 'center',
                width = 50,
                valign = 'middle',
                height = 4)
    """
    Menu to change the vocabulary used by the Wordle puzzle.
    """
    def changeVocabulary(self, button, filename = None):
        if filename == None:
            vocabulary_files = sorted(glob.glob('*.vocab'))
            if self._filename_vocabulary in vocabulary_files:
                vocabulary_files.remove(self._filename_vocabulary)
            if len(vocabulary_files) == 0:
                self._status = self._play_mode
                return
            menu_vocabularies = []
            for filename in vocabulary_files:
                button = urwid.Button(filename)
                urwid.connect_signal(button, 'click', self.changeVocabulary, filename)
                menu_vocabularies.append(urwid.AttrMap(button, None, focus_map = 'reversed'))
            self._menu = urwid.ListBox(urwid.SimpleFocusListWalker(menu_vocabularies))
            self._menu_height = min(10, len(vocabulary_files)) + 2
            dialog = urwid.LineBox(urwid.Padding(self._menu, left = 2, right = 2))
            self._loop.widget = urwid.Overlay(dialog,
                    self._background,
                    align = 'center',
                    width = 50,
                    valign = 'middle',
                    height = self._menu_height)
        else:
            dictionary = loadVocabulary(filename)
            if dictionary == {}:
                button.set_label(('highlight', filename))
                text, _ = self._status_bar.get_text()
                if ' ||' in text:
                    text = text[:text.find(' ||')]
                text += f" || {self._interface_strings['STATUS_CANNOT_OPEN']}."
                self._status_bar.set_text(text)
            else:
                self._dictionary = dictionary
                self._filename_vocabulary = filename
                self.restart()
    """
    Menu to change the type of game.
    """
    def changeGameType(self, button, selection = None):
        if selection == None:
            menu_gametypes = []
            menu_gametypes.append(urwid.Button(self._interface_strings['MENU_TYPE_NORMAL']))
            urwid.connect_signal(menu_gametypes[-1], 'click', self.changeGameType, self.Status.GAME);
            menu_gametypes.append(urwid.Button(self._interface_strings['MENU_TYPE_SOLVER']))
            urwid.connect_signal(menu_gametypes[-1], 'click', self.changeGameType, self.Status.SOLVER);
            self._menu = urwid.ListBox(urwid.SimpleFocusListWalker(menu_gametypes))
            self._menu_height = 2 + 2 # 2 Options + 2 extra rows.
            dialog = urwid.LineBox(urwid.Padding(self._menu, left = 2, right = 2))
            self._loop.widget = urwid.Overlay(dialog,
                    self._background,
                    align = 'center',
                    width = 50,
                    valign = 'middle',
                    height = self._menu_height)
        else:
            if selection != self._play_mode:
                self.restart()
            self._play_mode = selection
            self.changeMode(selection)
    """
    Menu to change the language of the interface.
    """
    def changeLanguage(self, button, filename = None):
        if filename == None:
            language_files = sorted(glob.glob('*.lang'))
            if self._filename_language in language_files:
                language_files.remove(self._filename_language)
            if len(language_files) == 0:
                self._status = self._play_mode
                return
            menu_languages = []
            for filename in language_files:
                button = urwid.Button(filename)
                urwid.connect_signal(button, 'click', self.changeLanguage, filename)
                menu_languages.append(urwid.AttrMap(button, None, focus_map = 'reversed'))
            self._menu = urwid.ListBox(urwid.SimpleFocusListWalker(menu_languages))
            self._menu_height = min(10, len(language_files)) + 2
            dialog = urwid.LineBox(urwid.Padding(self._menu, left = 2, right = 2))
            self._loop.widget = urwid.Overlay(dialog,
                    self._background,
                    align = 'center',
                    width = 50,
                    valign = 'middle',
                    height = self._menu_height)
        else:
            self._interface_strings = loadLanguage(filename)
            self._filename_language = filename
            self._status_bar.set_text(f' [ESC/F10] {self._interface_strings["STATUS_EXIT"]} | [F3] {self._interface_strings["STATUS_MENU"]}')
            if self._hints:
                if len(self._putative_words) < len(self._dictionary[self._word_length]):
                    if len(self._putative_words) > 2:
                        best_text  = f'{self._interface_strings["HINTS_SUGGESTIONS"]}: ' + ', '.join(self.suggestedWords(5)) + '\n'
                    else:
                        best_text  = f'{self._interface_strings["HINTS_SUGGESTIONS"]}: ' + ', '.join(self._putative_words) + '\n'
                    show_text = '-'*20 + '\n' + ', '.join(sorted(random.sample(list(self._putative_words), min(len(self._putative_words), 250))))
                    self._words_available.set_text([('highlight', best_text), ('text', show_text)])
                else:
                    show_text = ', '.join(sorted(random.sample(list(self._putative_words), min(len(self._putative_words), 300))))
                    self._words_available.set_text(('text', show_text))
            else:
                self._words_available.set_text('')
            self.changeMode(self._play_mode)
    """
    Add a new letter to the current active word
    """
    def addLetter(self, key):
        if self._status == self.Status.GAME:
            if self._active_row < self._number_of_attempts and self._active_char < self._word_length:
                if key == 'ç':
                    key = '&'
                self._letters_wrap[self._active_row][self._active_char].set_attr('normal')
                self._letters_values[self._active_row][self._active_char].set_text(key)
                self._active_char += 1
        else:
            if self._enter_letter:
                if self._active_row < self._number_of_attempts and self._active_char < self._word_length:
                    if key == 'ç':
                        key = '&'
                    self._letters_wrap[self._active_row][self._active_char].set_attr('unlabeled')
                    self._letters_values[self._active_row][self._active_char].set_text(key)
                    self._enter_letter = False
                    self._previous_bar = self._status_bar.get_text()[0]
                    self._status_bar.set_text('[1] Correct | [2] Wrong position | [3] Not present')
            else:
                if key == '1' or key == '2' or key == '3':
                    if key == '1':
                        self._letters_wrap[self._active_row][self._active_char].set_attr('good')
                    elif key == '2':
                        self._letters_wrap[self._active_row][self._active_char].set_attr('wrong')
                    elif key == '3':
                        self._letters_wrap[self._active_row][self._active_char].set_attr('missing')
                    self._status_bar.set_text(self._previous_bar)
                    self._enter_letter = True
                    self._active_char += 1
    """
    Remove a new letter to the current active word
    """
    def removeLetter(self):
        if not self._enter_letter:
            self._status_bar.set_text(self._status_bar.get_text()[0][:self._status_bar.get_text()[0].find(' ||')])
            self._letters_wrap[self._active_row][self._active_char].set_attr('normal')
            self._letters_values[self._active_row][self._active_char].set_text('')
            self._enter_letter = True
        elif self._active_row < self._number_of_attempts and self._active_char > 0:
            self._active_char -= 1
            self._letters_wrap[self._active_row][self._active_char].set_attr('normal')
            self._letters_values[self._active_row][self._active_char].set_text('')
    """
    Update the list of words from the dictionary that are still possible with
    the current set of clues.
    """
    def updatePutativeWords(self):
        putative_words = []
        for word in self._putative_words:
            valid = True
            present = True
            for i in range(self._word_length):
                valid = valid and word[i] in self._letters_possible[i]
            for must_letter in self._letters_present:
                present = present and must_letter in word
            if valid and present:
                putative_words.append(word)
        return putative_words
    """
    Return the list with the k-best words that can be used as the next guess. The
    words are selected by searching the words that their individuals characters
    maximize the entropy.
    """
    def suggestedWords(self, k):
        local_histogram = [dict([(x, 0) for x in self._valid_characters]) for i in range(self._word_length)]
        for word in self._putative_words:
            for i in range(self._word_length):
                local_histogram[i][word[i]] += 1
        entropy = dict(((x, 0) for x in self._valid_characters))
        for i in range(self._word_length):
            total = sum(local_histogram[i].values())
            for c in self._valid_characters:
                if local_histogram[i][c] != 0 and local_histogram[i][c] != total:
                    p = local_histogram[i][c] / total
                    local_histogram[i][c] = -(p * math.log2(p)
                                      + (1 - p) * math.log2(1 - p))
                    entropy[c] = max(entropy[c], abs(p * math.log(p)))
        suggested_words = []
        for word in self._dictionary[self._word_length]:
            if word in self._introduced_words:
                continue
            current_characters = dict(((c, 1) for c in self._valid_characters))
            score = 0
            for i in range(self._word_length):
                if ((word[i] not in self._letters_possible[i])
                or (len(self._letters_possible[i]) > 1)):
                    score += entropy[word[i]] * current_characters[word[i]]
                    current_characters[word[i]] = 0
            suggested_words.append((score, word))
        return map(operator.itemgetter(1), reversed(sorted(suggested_words)[-k:]))
    def getWord(self):
        word = ''
        for i in range(self._word_length):
            caracter = self._letters_values[self._active_row][i].get_text()[0]
            if caracter == '&':
                caracter = 'ç'
            word += caracter
        return word
    """
    Check if the current word is a valid word.
    """
    def checkWord(self):
        def finish():
            self._putative_words = self.updatePutativeWords()
            if self._hints:
                if len(self._putative_words) > 2:
                    best_text  = f'{self._interface_strings["HINTS_SUGGESTIONS"]}: ' + ', '.join(self.suggestedWords(5)) + '\n'
                else:
                    best_text  = f'{self._interface_strings["HINTS_SUGGESTIONS"]}: ' + ', '.join(self._putative_words) + '\n'
                show_text = '-'*20 + '\n' + ', '.join(sorted(random.sample(list(self._putative_words), min(len(self._putative_words), 250))))
                self._words_available.set_text([('highlight', best_text), ('text', show_text)])
            else:
                self._words_available.set_text('')
            if the_end:
                self._active_row = self._number_of_attempts
            else:
                self._active_row += 1
            self._active_char = 0
        if self._status == self.Status.GAME:
            current = self.getWord()
            if self._dictionary == {} or (self._word_length in self._dictionary and current in self._dictionary[self._word_length]):
                self._introduced_words.add(current)
                the_end = current == self._solution
                current = list(current)
                histogram = dict(self._solution_histogram)
                status = {}
                for i in range(self._word_length):
                    if self._solution[i] == current[i]:
                        histogram[current[i]] -= 1
                        self._letters_wrap[self._active_row][i].set_attr('good')
                        self._letters_possible[i] = set([current[i]])
                        self._letters_present.add(current[i])
                        status[current[i]] = 1
                        current[i] = '#'
                for i in range(self._word_length):
                    if current[i] != '#':
                        if current[i] in self._letters_possible[i]:
                            self._letters_possible[i].remove(current[i])
                        if current[i] in histogram and histogram[current[i]] > 0:
                            histogram[current[i]] -= 1
                            self._letters_wrap[self._active_row][i].set_attr('wrong')
                            self._letters_present.add(current[i])
                            status[current[i]] = 0
                        else:
                            self._letters_wrap[self._active_row][i].set_attr('missing')
                            for j in range(self._word_length):
                                if (current[i] in self._letters_possible[j]
                                    and not current[i] in self._letters_present):
                                    self._letters_possible[j].remove(current[i])
                            if not current[i] in status:
                                status[current[i]] = -1
                for c, s in status.items():
                    if s == 1:
                        self._keyboard[c].set_text(("kb_good", c))
                    elif s == 0:
                        self._keyboard[c].set_text(("kb_wrong", c))
                    else:
                        self._keyboard[c].set_text(("kb_miss", c))
                finish()
        else:
            current = self.getWord()
            self._introduced_words.add(current)
            for i in range(self._word_length):
                if self._letters_wrap[self._active_row][i].get_attr() == 'good':
                    self._letters_possible[i] = set([current[i]])
                    self._letters_present.add(current[i])
                    self._keyboard[current[i]].set_text(("kb_good", current[i]))
                elif self._letters_wrap[self._active_row][i].get_attr() == 'missing':
                    for j in range(self._word_length):
                        if (current[i] in self._letters_possible[j]
                            and not current[i] in self._letters_present):
                            self._letters_possible[j].remove(current[i])
                    if "keyboard" in str(self._keyboard[current[i]].get_text()[1]):
                        self._keyboard[current[i]].set_text(("kb_miss", current[i]))
                elif self._letters_wrap[self._active_row][i].get_attr() == 'wrong':
                    if current[i] in self._letters_possible[i]:
                        self._letters_possible[i].remove(current[i])
                    self._letters_present.add(current[i])
                    if not "kb_good" in str(self._keyboard[current[i]].get_text()[1]):
                        self._keyboard[current[i]].set_text(("kb_wrong", current[i]))
                the_end = len(self._putative_words) <= 1
            finish()

def main():
    if len(sys.argv) > 1:
        if sys.argv[1].lower() == 'game':
            mode = Interface.Status.GAME
        elif sys.argv[1].lower() == 'solver':
            mode = Interface.Status.SOLVER
    else:
        mode = Interface.Status.GAME
    word_length = 5
    interface = Interface(word_length = word_length, filename_vocabulary = 'catalan.vocab', filename_language = 'english.lang', game_mode = mode)
    interface.run()

if __name__ == '__main__':
    main()
